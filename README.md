A talk on GitLab CI/CD pipelines
--------------------------------

[![pipeline status](https://gitlab.gwdg.de/dmanik/gitlab-ci-talk/badges/master/pipeline.svg)](https://gitlab.gwdg.de/dmanik/gitlab-ci-talk/commits/master)

Grab the [freshest PDF](https://gitlab.gwdg.de/dmanik/gitlab-ci-talk/-/jobs/artifacts/master/raw/talk.pdf?job=makepdf).

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.